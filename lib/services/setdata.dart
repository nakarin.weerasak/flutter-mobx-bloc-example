import 'package:blocmobx/controllers/carcontroller.dart';
import 'package:blocmobx/models/cardata.dart';
import 'package:get_it/get_it.dart';

class SetDataService {
  final carController = GetIt.I.get<CarController>();
  setdata(Car car) {
    carController.setCarData(car);
  }
}