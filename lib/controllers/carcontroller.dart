import 'package:blocmobx/models/cardata.dart';
import 'package:mobx/mobx.dart';
part 'carcontroller.g.dart';

class CarController = _CarControllerBase with _$CarController;

abstract class _CarControllerBase with Store {

  @observable
  Car car;

  @action
  setCarData(Car _car) {
    car = _car;
  }
  
}